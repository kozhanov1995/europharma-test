<?php

namespace App\Exceptions;

use App\Mail\Sendmail;
use App\Models\Errors;
use App\Models\RecipientList;
use Carbon\Carbon;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Mail;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function report(Throwable $exception)
    {
        try{
            $error = new Errors();
            $error->time = Carbon::now();
            $error->message = $exception->getMessage();
            $error->file = $exception->getFile();
            $error->line = $exception->getLine();
            $error->user_id = backpack_user()->id;
            $error->save();

            parent::report($exception);

            $emailAddress = RecipientList::pluck('email')->toArray();

            Mail::to($emailAddress)->send(new Sendmail($error));
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }

    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        return parent::render($request, $exception);
    }
}
