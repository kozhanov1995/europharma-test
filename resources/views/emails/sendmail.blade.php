@component('mail::message')

    <div>
        <h2>Новая ошибка</h2>
        <p><b>Сообщение ошибки:</b> {{ $error->message }}</p>
        <p><b>Файл ошибки:</b> {{ $error->file }}</p>
        <p><b>Линия:</b> {{ $error->line }}</p>
        <p><b>Пользователь:</b> {{ $error->user_id }}</p>
    </div>


{{--@component('mail::button', ['url' => ''])
Button Text
@endcomponent--}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
