<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('recipient-list') }}'><i class='nav-icon la la-list'></i> Список получателей ошибок</a></li>
@php
    $count = \App\Models\Errors::where('status', 0)->count()
@endphp

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('errors') }}'><i class='nav-icon la la-question'></i> Ошибки <span class="badge badge-danger">{{ $count }}</span></a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('test') }}'><i class='nav-icon la la-archive'></i> Тест ошибки</a></li>
